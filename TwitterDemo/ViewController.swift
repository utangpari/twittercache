//
//  ViewController.swift
//  TwitterDemo
//
//  Created by Pari on 04/11/17.
//  Copyright © 2017 Pari. All rights reserved.
//

import UIKit
import TwitterKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
            if (session != nil) {
                print("signed in as \(String(describing: session?.userName))");
                self.performSegue(withIdentifier: "gotoTable", sender: self)
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
                
            }
        })
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func check(){
        Twitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("signed in as \(String(describing: session?.userName))");
            } else {
                print("error: \(String(describing: error?.localizedDescription))");
            }
        })
    }
//
//    func LogInMethod(){
//        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
//            if (session != nil) {
//                print("signed in as \(String(describing: session?.userName))");
//            } else {
//                print("error: \(String(describing: error?.localizedDescription))");
//            }
//        })
//        logInButton.center = self.view.center
//        self.view.addSubview(logInButton)
//        let  tblVC = MainTableViewController()
//        self.navigationController?.pushViewController(tblVC, animated: true)
//
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

